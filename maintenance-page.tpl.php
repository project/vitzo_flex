<?php

$img = '<img src="/img/logo/225x64-full.png" alt="'.t("Send us an e-mail").'">';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $language->language?>" lang="<?php echo $language->language?>">
  <head>
    <title><?php print $head_title ?></title>
	<style type="text/css">
		html, body {
			background-color: white;
			color: black;
			height: 100%;
			font: 12px/170% Arial, sans-serif;
		}
		img {
			border: none;
		}
	</style>
  </head>
  <body>

<html>
	<body>
	
	<div align="center">
	  <center>
	  <table width="100%" height="100%">
	    <tr>
			<td width="100%" style="text-align:center" valign="center">
		  
			<!-- Replace this with your own javascript encoded e-mail address to prevent spam -->
			<script type='text/javascript'><!--
				var v2="GQ5NBN8XNCVI2S";
				var v7=unescape(".%3FS%21%028Q%2C4%2Cx*%5D%3E");
				var v5=v2.length;var v1="";
				
				for(var v4=0;v4<v5;v4++){
					v1+=String.fromCharCode(v2.charCodeAt(v4)^v7.charCodeAt(v4));
				}
				document.write('<a href="javascript:void(0)" onclick="window.location=\'mail\u0074o\u003a'
					+v1+'?subject=Question%20%5Bwebsite%5D'+'\'"><?php echo $img?><\/a>');
			//--></script>
	
			<noscript><?php echo $img?></noscript>
			
			<div style="padding-top: 50px;">
				<?php print $content ?>
			</div>
			
			</td>
	    </tr>
	  </table>
	  </center>
	</div>

  </body>
</html>
