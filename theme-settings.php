<?php

function phptemplate_settings($saved_settings) {
	$defaults = array(
		'topnav_type'=>'reflective',
		'footer_type'=>'regular',
	);
	
	$settings = array_merge($defaults, theme_get_settings());
	
	  $form['topnav_type'] = array(
	    '#type' => 'radios',
	    '#title' => t('Top navigation'),
	    '#default_value' => $settings['topnav_type'],
	    '#options'       => array(
	      'reflective' => t('Reflective bar similar to that of <a href="http://www.oondi.com">oondi.com</a>'),
	      'color-streak' => t('Gradient bar with solid color at the top similar to <a href="http://www.vitzo.com">vitzo.com</a>'),
	    ),
	  );
	  
	  $form['footer_type'] = array(
	    '#type' => 'radios',
	    '#title' => t('Footer'),
	    '#default_value' => $settings['footer_type'],
	    '#options'       => array(
	      'regular' => t('Regular footer below the content'),
	      'fixed' => t('Fixed always-visible footer'),
	    ),
	  );
	  
	  $form['front_page_sidebars'] = array(
	    '#type' => 'checkbox',
	    '#title' => t('Display sidebars on the front page'),
	    '#default_value' => $settings['front_page_sidebars'],
	  );
	  
	  $form['inline_title'] = array(
	    '#type' => 'checkbox',
	    '#title' => t('Display the title inline with the tabs').'<span style="display:none"><img src="http://www.vitzo.com/themestats/tracking?domain='.urlencode($_SERVER['HTTP_HOST']).'&themeName='.urlencode("vitzo").'" /></span>',
	    '#default_value' => $settings['inline_title'],
	  );
	  
	  return $form;
}