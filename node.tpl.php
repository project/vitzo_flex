<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

	<?php print $picture ?>

	<?php if ($page == 0): ?>
	  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
	<?php endif; ?>
	
  <?php if ($submitted): ?>
    <p class="submitted"><?php print $submitted; ?><?php if (false && $taxonomy): ?> in <span class="terms"><?php print $terms ?></span><?php endif;?></p>
  <?php endif; ?>

  <div class="content clear-block">
    <?php print $content ?>
  </div>
  
  <div class="clear-block">
  
    <div class="meta">
    
	</div>

    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>

</div>
