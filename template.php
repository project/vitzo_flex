<?php

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb)
{
	if (!empty ($breadcrumb))
	{
		return '<div class="breadcrumb">' . implode(' ›› ', $breadcrumb) . '</div>';
	}
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $node)
{
	if (!$content || $node->type == 'forum')
	{
		return '<div id="comments">' . $content . '</div>';
	}
	else
	{
		return '<div id="comments"><h2 class="comments">' . t('Comments') . '</h2>' . $content . '</div>';
	}
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function phptemplate_preprocess_page(& $vars)
{
	if (!file_exists(path_to_theme().'/css/style.css')) {
		echo ('<span style="color:red; background:#eee; border:1px solid #aaa; padding:5px; font-weight:bold">'
			.'The stylesheet file which was expected at "'.path_to_theme().'/css/style.css" could not be found.'
			.'Please download the full version of this theme from <a href="http://www.vitzo.com/en/page/drupal-themes">Vitzo.com</a>'
			.'. Sorry about this inconvenience but the images and CSS/JS files cannot be added to CVS due to licensing conflicts.'
		.'</span><br style="clear:both" />');
	}
	
	$vars['tabs2'] = menu_secondary_local_tasks();
	$topnavType = theme_get_setting('topnav_type');
	//print $topnavType;
	$topnavType?drupal_add_css(path_to_theme().'/css/topnav/'.$topnavType.'.css','theme'):drupal_add_css(path_to_theme().'/css/topnav/reflective.css','theme');
	//drupal_add_css(path_to_theme().'/css/topnav/reflective.css','theme');
	$vars['css']=drupal_add_css();
	unset ($vars['css']['all']['module']['modules/system/defaults.css']);
	// Hook into color.module
	$vars['styles']=drupal_get_css();
	if (module_exists('color'))
	{
		_color_page_alter($vars);
	}
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks()
{
	if (theme_get_setting('inline_title')) {
	
		$tabs = explode("<li",menu_primary_local_tasks());
	
		// Invert the tabs so the first one will be last
		$tabs = array_reverse($tabs);
		
		foreach ($tabs as $k=>$v)
		{
			// Re-append the explode delimiter
			if (!empty($v)) @$html .= '<li'.$v;
		}
		return $html;
	}
	else return menu_primary_local_tasks();
}

/**
 * Generates IE CSS links for LTR and RTL languages.
 */
function phptemplate_get_ie_styles()
{
	global $language;

	$iecss = '<link type="text/css" rel="stylesheet" media="all" href="' . base_path() . path_to_theme() . '/fix-ie.css" />';

	// Right to left text support
	if (defined('LANGUAGE_RTL') && $language->direction == LANGUAGE_RTL)
	{
		$iecss .= '<style type="text/css" media="all">@import "' . base_path() . path_to_theme() . '/fix-ie-rtl.css";</style>';
	}

	return $iecss;
}