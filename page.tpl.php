<?php
$topnavType = theme_get_setting('topnav_type');
$footerType = theme_get_setting('footer_type');
$disableSideBars = !theme_get_setting('front_page_sidebars') && $is_front;
$facebook = theme_get_setting('facebook_compatible');

if ($disableSideBars)
{
	$body_classes = str_replace('two-sidebars','',$body_classes);
	$body_classes = str_replace('one-sidebar','',$body_classes);
	$body_classes = str_replace('sidebar-left','',$body_classes);
	$body_classes = str_replace('sidebar-right','',$body_classes);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $language->language?>" lang="<?php echo $language->language?>" <?php echo $facebook?'xmlns:fb="http://www.facebook.com/2008/fbml':''?>>
<head>
	<title><?php echo $head_title?></title>
	<?php
		/*drupal_set_css($path = NULL, $type = 'module', $media = 'all', $preprocess = TRUE)
		$styles=drupal_get_css();*/
		//print_r(get_defined_vars());
	/*	$arr = variable_get("color_vitzo_files",$base_path.$directory);
		if(isset($arr[0])) $pieces = explode("/",$arr[0]);
		array_splice($pieces, -1, 1);
		print_r($pieces);*/
	?>
	<?php/*<link rel="stylesheet" href="<?php echo $base_path.$directory; ?>/css/topnav/<?php echo $topnavType?$topnavType:'reflective'?>.css" type="text/css" />*/?>
	<link rel="stylesheet" href="<?php echo $base_path.$directory; ?>/css/footer/<?php echo $footerType?$footerType:'regular'?>.css" type="text/css" />
	<?php echo $head.$styles.$scripts?>
	<!--[if IE 6]> 
		<style type="text/css">
			.topnav-reflective #topnav a {
				width: 0px; 
			}
		</style>
	<![endif]--> 
	<!--[if IE 7]> 
		<style type="text/css">
			.links a.translation-link {
				padding-top: 1px;
			}
			.links .print-page, .links .print-pdf {
				padding-top: 4px;
			}
			.icon-list li  {
				padding-left: 18px;
			}
		</style>
	<![endif]-->  
</head>

<body class="<?php echo $body_classes?> topnav-<?php echo $topnavType?$topnavType:'reflective'?> footer-<?php echo $footerType?$footerType:'regular'?>">

<div id="container-wrapper">
	<div id="container">
		
		<div id="top-containers">
			<div id="logo-container">
				<a href="<?php echo $front_page?>" title="<?php echo $site_name?>"><img src="<?php echo $logo?>" alt="<?php echo $site_name?>" /></a>
			</div>
			<div id="slogan-container">
				<div id="site-name"><?php echo $site_name?></div>
				<?php echo $site_slogan?>
			</div>
			<?php if ($search_box):?>
			<div id="search-container">
				<?php echo $search_box?>
			</div>
			<?php endif;?>
		</div>
		
		<div id="topnav">
			<?php if (false && !empty($primary_links)):?>
				<?php echo theme('links', $primary_links, array('class'=>'nav'))?>
			<?php endif;?>
		    <?php
			$menu=menu_tree_all_data('primary-links');
     		print "\n<ul class=\"nav\">\n";
     		if (in_array(theme_get_setting('topnav_type'),array('reflective'))) {
     			print "<li>&nbsp;</li>";
			}
			foreach ($menu as $menu_item) {
              print "\n<li>".l($menu_item['link']['title'],$menu_item['link']['href']); 
 			  if (in_array(theme_get_setting('topnav_type'),array('color-streak')) && (!empty($menu_item['link']['has_children'])) && (!empty($menu_item['below']))) 
		    	{
		    	 print "\n<ul class=\"subnav\">";
		    	 foreach ($menu_item['below'] as $child) 
	    		 	print "\n<li>".l($child['link']['title'],$child['link']['href'])."</li>";
		    	 print "\n</ul>\n";
		    	}
			  print "</li>\n";
		    }
			print "\n</ul>\n";
			?>
		</div>
		
		<div id="contents-container">
		
			<?php if(!$disableSideBars): ?>
			<div id="sidebar-left" class="sidebar">
				<?php/* ALTERNATIVE-STYLE LINKS
				<h6>TITLE</h6>
				<ul class="sidenav-links">		
					<li><a rel="nofollow" href="#">LINK</a></li>
				</ul>
				*/?>
				<?php echo $left?>
			</div>
			<?php endif; ?>
			
			<div id="contents">
				<?php if (!$is_front) echo $breadcrumb; ?>
				<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif;?>
				<?php if ($tabs): ?>
					<?php if(theme_get_setting('inline_title')):?>
						<div class="tabs-primary tabs-right">
							<h1 id="in-tabs"><?php echo $title?></h1>
							<ul>
								<?php echo $tabs?>
							</ul>
						</div>
					<?php else:?>
						<h1 id="with-tabs"><?php echo $title?></h1>
						<div class="tabs-primary tabs-left">
							<ul>
								<?php echo $tabs?>
							</ul>
						</div>
					<?php endif;?>
				<?php else:?>
					<h1><?php echo $title?></h1>
				<?php endif;?>
				
				<?php if ($tabs2): print '<div class="tabs-secondary"><ul>'. $tabs2 .'</ul></div>'; endif;?>
				<?php if($messages): print $messages; endif;?>
				<?php echo $header?>
				<div id="content">
				<?php echo $content; ?>
				</div>
			</div>
			<?php if ($right && !$disableSideBars):?>
			<div id="sidebar-right" class="sidebar">
				<?php echo $right?>
			</div>
			<?php endif;?>
		</div>
		
		<!-- Required when page exceeds viewport -->
		<span style="display:none">Lt6Si99x</span><br class="clear" />
		
	</div>
	<div id="footer">
		<div id="footer-contents">
			<div id="footer-text">
				<?php echo $footer?>
				<?php if (!empty($secondary_links)):?>
					<?php echo theme('links', $secondary_links, array('id'=>'footer-items'))?>
				<?php endif;?>
				<?php echo $footer_message?>
				<?php if (!preg_match('/vitzo.com$/',$_SERVER["HTTP_HOST"])):?>
					<!-- IN ORDER TO USE THIS THEME THESE LINKS MUST REMAIN INTACT (COPYRIGHT 2008-2009 @ VITZO LIMITED) -->
					<br /><a href="http://www.vitzo.com/page/drupal-themes" title="Drupal theme by Vitzo Internet Services">Drupal theme development</a> by <a href="http://www.vitzo.com/" title="Vitzo Internet Services">Vitzo</a>
				<?php endif;?>
			</span>
		</div>
	</div>
</div>
<?php echo $closure?>
<?php echo $facebook?'div id="FB_HiddenIFrameContainer" style="display:none; position:absolute; left:-100px; top:-100px; width:0px; height: 0px;"></div>':''?>
</body>
</html>